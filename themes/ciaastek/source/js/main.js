const contentDivs = document.querySelectorAll('.content')
Array.from(contentDivs).forEach(content => {
  const paragraphs = content.querySelectorAll('p')
  Array.from(paragraphs).forEach(paragraph => {
    const elements = Array.from(paragraph.children)
    const imageElements = elements.filter(child => child instanceof HTMLImageElement)

    if (imageElements.length > 0 && elements.length === imageElements.length) {
      paragraph.classList.add('gallery')
    }
  })
})
